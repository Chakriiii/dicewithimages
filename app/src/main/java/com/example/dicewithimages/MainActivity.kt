package com.example.dicewithimages

import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button: Button = findViewById(R.id.button)
        button.setOnClickListener {
            rolldice1()
            rollDice2()
        }

    }

    private fun rollDice2() {
        val nsides = Dice(6)
        val rdice = nsides.roll()

        val image: ImageView =  findViewById(R.id.iv2) //finding imageview


        //giving the image to display according to the result
        //when = switch case
        when (rdice) {
            1 -> image.setImageResource(R.drawable.dice_1)
            2 -> image.setImageResource(R.drawable.dice_2)
            3 -> image.setImageResource(R.drawable.dice_3)
            4 -> image.setImageResource(R.drawable.dice_4)
            5 -> image.setImageResource(R.drawable.dice_5)
            6 -> image.setImageResource(R.drawable.dice_6)
        }
        image.contentDescription = rdice.toString()

        val rtext: TextView = findViewById(R.id.tv2)
        rtext.text =rdice.toString()

    }

    private fun rolldice1() {
        val nsides = Dice(6)
        val rdice = nsides.roll()

        val image: ImageView = findViewById(R.id.iv1)

        when (rdice) {
            1 -> image.setImageResource(R.drawable.dice_1)
            2 -> image.setImageResource(R.drawable.dice_2)
            3 -> image.setImageResource(R.drawable.dice_3)
            4 -> image.setImageResource(R.drawable.dice_4)
            5 -> image.setImageResource(R.drawable.dice_5)
            6 -> image.setImageResource(R.drawable.dice_6)
        }
        image.contentDescription = rdice.toString()

        val rtext: TextView = findViewById(R.id.tv1)
        rtext.text =rdice.toString()
    }
}

class Dice(val sides: Int) {
    fun roll(): Int{
    return (1..sides).random()
    }
}